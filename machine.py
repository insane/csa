import sys

from isa import *
import logging

logger = logging.getLogger("machine_logger")
logger.setLevel(logging.INFO)
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)
formatter = logging.Formatter("%(message)s")
console_handler.setFormatter(formatter)
logger.addHandler(console_handler)


class ALU:
    def __init__(self):
        self.left = 0
        self.right = 0
        self.N = 0
        self.Z = 1
        self.C = 0

    def set_flags(self, res):
        self.N = 1 if res < 0 else 0
        self.Z = 1 if res == 0 else 0

    @staticmethod
    def invert_string(s):
        return "".join(["1" if c == "0" else "0" for c in s])

    def to_unsigned(self, a):
        return int(self.invert_string(bin(abs(a))[2:].zfill(WORD)), 2) + 1

    def to_signed(self, a):
        self.C = 1 if a >= U_MAX_VALUE else 0
        a = a if self.C == 0 else a % U_MAX_VALUE
        return a if MAX_VALUE > a >= -MAX_VALUE else -self.to_unsigned(a)

    def add(self, a, b):
        a = a if a >= 0 else self.to_unsigned(a)
        b = b if b >= 0 else self.to_unsigned(b)
        return self.to_signed(a + b)

    def sub(self, a, b):
        a = a if a >= 0 else self.to_unsigned(a)
        b = b if b >= 0 else self.to_unsigned(b)
        return self.add(a, self.to_unsigned(b))

    def div(self, a):
        self.C = a % 2
        return a // 2

    def calc_op(self, left, right, op_type):
        if op_type == "add":
            return self.add(left, right)
        elif op_type == "sub" or op_type == "cmp":
            return self.sub(left, right)
        raise Exception("Incorrect binary operation")

    def calc_nop(self, res, op_type):
        if op_type == "asl":
            return self.add(res, res)
        elif op_type == "asr":
            return self.div(res)
        elif op_type == "inc":
            return self.add(res, 1)
        elif op_type == "dec":
            return self.sub(res, 1)
        raise Exception("Incorrect unary operation")

    def calc(self, left, right, op_type, change_flags=False):
        is_left_char = True if isinstance(left, str) else False
        left = ord(left) if is_left_char else int(left)
        C = self.C

        if right is None:
            is_right_char = False
            res = self.calc_nop(left, op_type)
        else:
            is_right_char = True if isinstance(right, str) else False
            right = ord(right) if is_right_char else int(right)
            res = self.calc_op(left, right, op_type)
        if change_flags:
            self.set_flags(res)
        else:
            self.C = C
        if is_left_char or is_right_char:
            res = chr(res)
            if is_left_char:
                left = chr(left)
        return left if op_type == "cmp" else res


class DataPath:
    registers = {"AC": 0, "AR": 0, "IP": 0, "PC": 0, "PS": 0, "DR": 0, "CR": 0}
    memory = []
    alu = ALU()

    def __init__(self):
        self.mem_size = MAX_ADDR
        self.memory = [{"value": 0}] * self.mem_size
        self.registers["AC"] = 0
        self.registers["PS"] = 1  # self.Z = 1
        self.output_buffer = []
        self.ports = {1: [], 2: [], 3: [], 4: [], 5: [], 6: []}

    def get_reg(self, reg):
        return self.registers[reg]

    def set_reg(self, reg, val):
        self.registers[reg] = val

    def write(self):
        self.memory[self.registers["AR"]] = {"value": self.registers["DR"]}

    def read(self):
        self.registers["DR"] = self.memory[self.registers["AR"]]["value"]


class ControlUnit:
    def __init__(self, program, data_path, start_address, input_data, limit):
        self.program = program
        self.data_path = data_path
        self.limit = limit
        self.instr_counter = 0
        self.input_data = input_data
        self.input_data_address = program[len(program) - 1]["index"] + 1

        self.sig_latch_reg("IP", start_address)

        self._tick = 0
        self._map_instruction()

    def tick(self):
        self._tick += 1

    def current_tick(self):
        return self._tick

    def sig_latch_reg(self, reg, val):
        self.data_path.set_reg(reg, val)

    def get_reg(self, reg):
        return self.data_path.get_reg(reg)

    def _map_instruction(self):
        for instr in self.program:
            self.data_path.memory[int(instr["index"])] = instr

    def calc(self, left, right, opcode, change_flags=False):
        res = self.data_path.alu.calc(left, right, opcode, change_flags)
        if change_flags:
            self.sig_latch_reg("PS", self.get_reg("PS") ^ ((self.get_reg("PS") ^ self.data_path.alu.C) & 1))
            self.sig_latch_reg(
                "PS", self.get_reg("PS") ^ ((self.get_reg("PS") ^ (self.data_path.alu.Z << 1)) & (1 << 1))
            )
            self.sig_latch_reg(
                "PS", self.get_reg("PS") ^ ((self.get_reg("PS") ^ (self.data_path.alu.N << 2)) & (1 << 2))
            )
        return res

    def sig_write(self):
        self.data_path.write()

    def sig_read(self):
        self.data_path.read()

    def decode_and_execute_instruction(self):
        comment = ""

        # цикл выборки команды
        self.sig_latch_reg("AR", self.calc(0, self.get_reg("IP"), "add"))
        self.sig_latch_reg("IP", self.calc(1, self.get_reg("IP"), "add"))
        self.sig_latch_reg("CR", self.data_path.memory[int(self.get_reg("AR"))])
        instruction = self.get_reg("CR")
        opcode = instruction["opcode"]

        if "operand" in instruction.keys():
            self.sig_latch_reg("DR", int(self.get_reg("CR")["operand"]))

            # цикл выборки адреса
            if instruction["address"]:
                self.sig_latch_reg("AR", self.calc(0, int(self.get_reg("DR")), "add"))
                self.sig_read()

            # цикл выборки операнда
            self.sig_latch_reg("AR", self.calc(0, int(self.get_reg("DR")), "add"))
            self.sig_read()

            if opcode == "load":
                self.sig_latch_reg("AC", self.calc(0, self.get_reg("DR"), "add", True))
                self.tick()
                comment = "DR -> AC"

            elif opcode == "store":
                self.sig_latch_reg("DR", self.calc(0, self.get_reg("AC"), "add"))
                self.tick()
                self.sig_write()
                self.tick()
                comment = "AC -> DR, DR -> mem[AR]"

            elif opcode == "out":
                self.data_path.output_buffer.append(self.get_reg("AC"))
                self.data_path.ports[self.get_reg("AR")].append(self.get_reg("AC"))
                self.tick()
                comment = "AC -> #DR"

            elif opcode == "in":
                self.sig_latch_reg("AC", self.calc(0, self.input_data[0], "add"))
                self.tick()
                self.data_path.ports[self.get_reg("AR")].append(self.input_data[0])
                self.input_data = self.input_data.replace(self.input_data[0], "", 1)
                comment = "#DR -> AC"

            elif opcode in branch_commands:
                ind = branch_commands.index(opcode)
                flag = branch_flags[ind]
                condition = True

                if (flag is not None) and flag[0] == "!":
                    condition = eval("not self.data_path.alu." + flag[1])
                    self.tick()
                elif flag is not None:
                    condition = eval("self.data_path.alu." + flag[0])
                    self.tick()
                if condition:
                    self.sig_latch_reg("IP", self.calc(0, self.get_reg("AR"), "add"))
                    comment = "AR -> IP"
                    self.tick()
            else:
                # арифметическая операция
                self.sig_latch_reg("AC", self.calc(self.get_reg("AC"), self.get_reg("DR"), opcode, True))
                self.tick()
        else:
            if opcode == "hlt":
                return False
            elif opcode == "cla":
                self.sig_latch_reg("AC", self.calc(self.get_reg("AC"), self.get_reg("AC"), "sub", True))
            elif opcode == "nop":
                pass
            else:
                # унарная операция
                self.sig_latch_reg("AC", self.calc(int(self.get_reg("AC")), None, opcode, True))
                self.tick()
        self.__print__(comment)
        return True

    def command_cycle(self):
        while self.instr_counter < self.limit:
            is_next_command = self.decode_and_execute_instruction()
            if not is_next_command:
                return
            self.instr_counter += 1

    @staticmethod
    def __print_symbol__(text):
        return str((lambda x: ord(x) if isinstance(x, str) else x)(text))

    def __print__(self, comment):
        state_repr = (
            "TICK: {:4} | AC {:7} | IP: {:4} | AR: {:4} | PS: {:3} | DR: {:7} | mem[AR] {:7} | " "CR: {:12} |"
        ).format(
            self._tick,
            self.__print_symbol__(self.get_reg("AC")),
            str(self.get_reg("IP")),
            str(self.get_reg("AR")),
            str(bin(self.get_reg("PS"))[2:].zfill(3)),
            self.__print_symbol__(self.get_reg("DR")),
            self.__print_symbol__(self.data_path.memory[self.get_reg("AR")]["value"]),
            self.get_reg("CR")["opcode"]
            + (lambda x: " " + str(x["operand"]) if "operand" in x.keys() else "")(self.get_reg("CR")),
        )
        logger.info(state_repr + " " + comment)


def simulate(code, limit, input_data, start_addr):
    data_path = DataPath()
    control_unit = ControlUnit(code, data_path, start_addr, input_data, limit)
    control_unit.command_cycle()
    return [control_unit.data_path.output_buffer, control_unit.instr_counter, control_unit.current_tick()]


def main(code, input_f):
    with open(input_f, encoding="utf-8") as file:
        input_text = file.read()
        input_text += "\0"
    start_addr, code = read_code(code)
    output, instr_num, ticks = simulate(
        code,
        limit=500000,
        input_data=input_text,
        start_addr=start_addr,
    )
    print(f"Output: {''.join(map(str, output))}\nInstruction number: {instr_num}\nTicks: {ticks}")


if __name__ == "__main__":
    assert len(sys.argv) == 3, "Wrong arguments: machine.py <code_file> <input_file>"
    _, code_file, input_file = sys.argv
    # code_file = "examples/target.txt"
    # input_file = "examples/input/cat.txt"
    d = DataPath()
    main(code_file, input_file)
