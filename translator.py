import re
import sys

from isa import *

labels = {}
start_address = 0


def parse_assembly_code(assembly_code):
    parsed_code = []
    pc = 0
    global start_address

    lines = assembly_code.split("\n")
    for line in lines:
        line = line.split(";")[0].strip()
        if line:
            if re.match(r".+\'.+", line):
                parts = re.split(r" ", line, maxsplit=1)
            else:
                parts = re.split(r" ", line)
            parts = list(filter(None, parts))
            parsed_code.append(parts)
            if re.match("org", parts[0]):
                pc = int(parts[1]) - 1
                start_address = int(parts[1])
            elif re.match(".+:", parts[0]):
                labels[parts[0]] = pc
                if len(parts) > 1 and re.match("'.+", parts[1]):
                    pc += int(len(parts[1]))
            pc += 1
    return parsed_code


def is_indirect_addr(addr):
    if re.match(r"\(.+\)", addr):
        return True
    return False


def make_op_string(arr, index):
    is_indirect = is_indirect_addr(arr[1])
    if is_indirect:
        arr[1] = arr[1].replace("(", "").replace(")", "")
    for label in labels:
        if arr[1] == label.replace(".", "").replace(":", ""):
            return [
                {"index": index, "opcode": arr[0], "operand": int(labels[label]), "value": 0, "address": is_indirect}
            ]
    if len(arr) == 2:
        return [{"index": index, "opcode": arr[0], "operand": int(arr[1]), "value": 0, "address": is_indirect}]


def make_nop_string(arr, index):
    return [{"index": index, "opcode": arr[0], "value": 0}]


def make_const_string(arg, index):
    lines = []
    arg = arg.replace("'", "")
    for label in labels:
        if arg == label.replace(".", "").replace(":", ""):
            lines.append({"index": index, "value": labels[label], "opcode": "nop"})
            return lines
    if re.match(r"^\d+$", arg):
        lines.append({"index": index, "value": int(arg), "opcode": "nop"})
        return lines
    for letter in arg:
        if letter == "\\":
            lines.append({"index": index, "value": "\0", "opcode": "nop"})
            index += 2
            return lines
        else:
            lines.append({"index": index, "value": letter, "opcode": "nop"})
        index += 1
    return lines


def source2json(arr, index):
    if re.match(r".+:", arr[0]):
        if is_op_command(arr[1]):
            return make_op_string(arr[1:], index)
        elif is_nop_command(arr[1]):
            return make_nop_string(arr[1:], index)
        else:
            return make_const_string(arr[1], index)
    else:
        if is_op_command(arr[0]):
            return make_op_string(arr, index)
        elif is_nop_command(arr[0]):
            return make_nop_string(arr, index)


def translate(source_code):
    translated = []
    buf = []
    pc = 0
    parsed_code = parse_assembly_code(source_code)
    for code in parsed_code:
        if not re.match("org", code[0]):
            buf = source2json(code, pc)
            for b in buf:
                translated.append(b)
        else:
            pc = int(code[1]) - 1
        if re.match(".+:", code[0]):
            if len(code) > 1 and re.match("'.+", code[1]):
                pc += int(len(code[1]))
        pc += 1
    # for tr in translated:
    #    print(tr)
    return translated


def main(source_code, code_target):
    with open(source_code, encoding="utf-8") as f:
        source_code = f.read()
    code = translate(source_code)
    write_code(code_target, start_address, code)
    global labels
    labels = {}
    print("source LoC:", len(source_code), "code instr:", len(code))


if __name__ == "__main__":
    assert len(sys.argv) == 3, "Wrong arguments: translator.py <input_file> <target_file>"
    _, source, target = sys.argv
    # source = "examples/asm/prob5.asm"
    # target = "examples/target.txt"
    main(source, target)
