# Lab3. Транслятор и модель

- P33111, Привалов Ярослав.
- `asm | acc | neum | hw | instr | struct | stream | port | cstr | prob5| [4]char`
- Без усложнения
- Вопросы 
- - `Два входа в регистры DR и AC?`
- - `Память статическая?`

## Язык программирования

### Синтаксис

**Форма Бэкуса-Наура:**

```ebnf
<программа> ::= <строка_программы> | <строка_программы> <программа>
<строка_программы> ::= <адрес> | [<метка>] <адресная команда> <операнд> | 
[<метка>] <безадресная команда> | [<метка>] <метка константы> <константа> | <пустая строка> 

<метка> ::= <слово>
<адресная команда> = add | load | store | cmp | sub | jmp | in | out
<безадресная команда> ::= hlt | cla | asl | asr | dec | inc | nop
<операнд> ::= <число> | <метка>
<константа> ::= <число> | '<слово>'
<слово> ::= <символ> | <слово> <символ>
<число> ::= <цифра> | <число> <цифра>
<цифра> ::= 0| 1 | 2 | .. | 8 | 9
<символ> ::= a | b | c | ... | z | A | B | C | ... | Z | <цифра>
```

**Пояснение:**

Каждая непустая строка программы это одно из нижеперечисленных:

* **адресная команда**
    * может иметь метку в начале
    * указывается название команды и адрес операнда через пробел
* **безадресная команда**
    * может иметь метку в начале
    * указывается только название команды
* **константа**
    * указывается метка и после неё константа
    * константа может быть 32-х битным числом
    * константа может быть строкой: конец строки должен содержать нультерминатор
* **адрес**
    * указывается специальное слово `org` и адрес в десятичном формате

Пример программы, печатающей `Hello world!` в консоль
```asm
org 2                           ;размещаем данные начиная с адреса 2
hello: 'Hello world!\0' 
addr: hello                     ;записываем указатель на начало строки в переменную
null_terminator: '\\0'

org 25
.start: load (addr)             ;загружаем в аккумулятор символ через косвенную адресацию
        cmp null_terminator     ;устанавливаем флаги результата разности аккумулятора и нультерминатора
        jmz end                 ;нашли нультерминатор? прыгаем на остановку
        out 5                   ;подаём на вход устройства, подключённого к порту 5, значение аккумулятора
        load addr               ;подготавливаем адрес следующего символа
        inc
        store addr
        jmp start               ;переходим на start
    .end: hlt                   ;остановить программу
```

**Семантика**

- Видимость данных -- глобальная
- Поддерживаются целочисленные литералы, находящиеся в диапазоне от $`-2^{31}`$ до $`2^{31}-1`$
- Поддерживаются строковые литералы, символы строки необходимо заключить в кавычки, в конце строки необходимо указать символ `/0`
- Код выполняется последовательно
- Программа обязательно должна включать метку `.start:`, указывающую на 1-ю выполняемую интсрукцию. Между меткой `.start:` и командой `org` не должно быть никаких других инструкций и данных.
- Название метки не должно совпадать с названием команды и не может начинаться с цифры.
- Метки находятся на одной строке с командами, операнды находятся на одной строке с командами.
- Пустые строки игнорируются, количество пробелов в начале и конце строки не важно.
- Любой текст, расположенный в конце строки после символа `';'` трактуется как комментарий.

Память выделяется статически, при запуске модели.

## Организация памяти

* Память команд и данныx --- общая
* Размер машинного слова --- `32` бит
* Память содержит `2^11` ячеек
* Командам ввода вывода доступны порты с `1` по `6`

* Поддерживаются следующие **виды адресаций**:
    * **Прямая**: в качестве аргумента команды передается адрес ячейки, значение в которой будет использовано как
      операнд.
      Например, если `mem[50] = 25`, то команда `add 50` обозначает, что к значению в аккумуляторе добавится число 25.

    * **Косвенная**: в качестве аргумента команды передается адрес, по которому лежит адрес операнда.
      Например, если `mem[50] = 25`, `mem[33] = 50`, то команда `add (33)` также обозначает, что к аккумулятору
      добавится значение 25.


* Существует несколько **регистров**:
    * Аккумулятор (AC): в него записываются результаты всех операций. Также к нему подключены входы/выходы портов ввода-вывода
    * Счетчик команд (IP): хранит адрес следующей выполняемой команды
    * Регистр состояния (PS): хранит состояние после исполнения каждой инструкции

      ```
        | N | Z | C |
          2   1   0
      ```
        * 0-й бит хранит значение флага C
        * 1-й бит хранит значение флага Z
        * 2-й бит хранит значение флага N


* Регистр данных (DR): хранит данные для записи в память и считывания из памяти
* Регистр адреса (AR): хранит адрес последней ячейки в памяти, к которой было обращение

## Система команд

Особенности процессора:

- Машинное слово -- `32` бита, знаковое.
- В качестве аргументов команды принимают `11` битные беззнаковые адреса

Каждая команда выполняется в несколько циклов:

1. Цикл выборки команды: по адресу из счетчика команд из памяти достается команда

- `IP -> AR, IP + 1 -> IP, mem[AR] -> DR, DR -> CR`

2. Цикл выборки адреса (для инструкций с косвенной адресацией): `CR[addr] -> DR, DR -> AR`

3. Цикл выборки операнда (для адресных команд): в регистр данных помещается адрес операнда, регистр данных передаётся в
   регистр адреса, из памяти в регистр данных записывается значение операнда

- `CR[addr] -> DR, DR -> AR, mem[AR] -> DR`

4. Цикл исполнения: совершаются действия, необходимые для выполнения команды. Результаты вычисления записываются в
   аккумулятор

### Набор инструкций

| Язык  | Адресная | Ветвление | Описание                                                                                       |
|:------|:---------|-----------|:-----------------------------------------------------------------------------------------------|
| load  | +        | -         | загрузить значение из заданной ячейки                                                          |
| store | +        | -         | загрузить значение в заданную ячейку                                                           |
| in | +        | -         | загрузить значение из #DR выбранного устройства в AC                                                           |
| out | +        | -         | загрузить значение AC в #DR выбранного устройства    |
| add   | +        | -         | добавить значение из заданной ячейки к аккумулятору                                            |
| sub   | +        | -         | вычесть значение из заданной ячейки из аккумулятора                                            |
| jmp   | +        | +         | перейти в заданную ячейку                                                                      |
| cmp   | +        | -         | выставить флаги как результат вычитания заданной ячейки из аккумулятора, сохранить аккумулятор |
| jmn   | +        | +         | перейти в заданную ячейку если N = 1                                                           |
| jmnn  | +        | +         | перейти в заданную ячейку если N = 0                                                           |
| jmc   | +        | +         | перейти в заданную ячейку если C = 1                                                           |
| jmnc  | +        | +         | перейти в заданную ячейку если C = 0                                                           |
| jmz   | +        | +         | перейти в заданную ячейку если Z = 1                                                           |
| jmnz  | +        | +         | перейти в заданную ячейку если Z = 0                                                           |
| asl   | -        | -         | сдвинуть значение в аккумуляторе влево, AC[31] -> C                                            |
| asr   | -        | -         | сдвинуть значение в аккумуляторе вправо, AC[0] -> C                                            |
| dec   | -        | -         | уменьшить значение в аккумуляторе на 1                                                         |
| inc   | -        | -         | увеличить значение в аккумуляторе на 1                                                         |
| cla   | -        | -         | очистить аккумулятор (записать в него 0)                                                       |
| hlt   | -        | -         | остановить работу программы                                                                    |                                                                         |
| nop   | -        | -         | отсутствие операции                                                                            |



### Кодирование инструкций

- Машинный код сереализуется в список JSON.
- Один элемент списка, одна инструкция или константа.

Пример сереализованной команды `load 2` и константы 10:

```json
[
  {
    "index": 15,
    "opcode": "load",
    "operand": 2,
    "value": 0,
    "address": False
  },
  {
    "index": 2,
    "value": 10,
    "opcode" : "nop"
  }
]
```

где:

- `index` -- адрес объекта в памяти;
- `opcode` -- код оператора, идентификатор команды; У констант для упрощения всегда "nop"
- `operand` -- аргумент команды, адрес ячейки над которой совершается операция. Отсуствует у безадресных команд и
  констант;
- `address` -- логическое поле, имеющее значение True, если адресация косвенная, и False, если прямая.
- `value` -- значение ячейке с адресом index, если она будет интерпретирована как данные. Так как команды сериализуются
  в высокоуровневую структуру, было принято решение установить у команд это значение в 0.

## Транслятор

Интерфейс командной строки: `translator.py <input_file> <target_file>`

Реализовано в модуле: [translator](./translator.py)

Этапы трансляции (функция `translate`):

1. Выделение меток из кода, проверка их корректности (не совпадают с названиями команд, отсуствуют дубликаты)
2. Парсинг кода построчно, определение типа команды (адресная, безадресная, константа)
3. Генерация машинного кода в зависимости от типа команды

Правила генерации машинного кода:

- Метки не сохраняются в машинном коде. Метки, использованные в качестве операнда, преобразуются к адресам команд

## Модель процессора

Интерфейс командной строки: `machine.py <machine_code_file> <input_file>`

Реализовано в модуле: [machine](./machine.py).

### DataPath

Реализован в классе `DataPath`.

![Data Path](./processor.png)


Сигналы (реализованы в виде методов класса):

- `set_reg` -- защёлкнуть выбранное значение в регистре с указанным именем
- `read` --- считать данные из `mem[AR]` в регистр `DR`
- `write` --- записать данные из регистра `DR` в `mem[AR]`

В виде отдельного класса реализовано арифметико-логическое устройство (АЛУ)

- в данном классе реализован метод `calc`, принимающий аргументы с одного или двух входов и совершающий над ними
  арифметико-логическую операцию
- в результате выполнения операций устанавливаются следующие флаги
    - `Z` -- значение в аккумуляторе равно 0
    - `N` -- значение в аккумуляторе отрицательно
    - `C` -- произошло переполнение

### ControlUnit

Реализован в классе `ControlUnit`.

![Control Unit](./control_unit.png)

- Метод `decode_and_execute_instruction` моделирует выполнение полного цикла инструкции (цикл выборки инструкции,
  операнда, исполнения)
- В рамках реализованной модели на python существуют счетчик количества инструкций только для наложения ограничения на  кол-во шагов моделирования

Особенности работы модели:

- Цикл симуляции осуществляется в функции `simulation`.
- Шаг моделирования соответствует одной инструкции процессора с выводом состояния в журнал.
- Для журнала состояний процессора используется стандартный модуль `logging`.
- Количество инструкций для моделирования лимитировано.
- Остановка моделирования осуществляется при:
    - превышении лимита количества выполняемых инструкций;
    - если выполнена инструкция `hlt`.

## Тестирование

Реализованные програмы

1. [hello world](examples/asm/hello.asm): вывести на экран строку `'Hello World!'`
2. [cat](examples/asm/cat.asm): программа `cat`, повторяем ввод на выводе.
3. [hello_user_name](examples/asm/hello_user_name.asm) -- программа `hello_username`: запросить у пользователя его
   имя, считать его, вывести на экран приветствие
4. [prob5](examples/asm/prob5.asm): найти наименьший общий делитель для всех чисел от 1 до 10.

Интеграционные тесты реализованы тут [golden_test](./golden_test.py):

- через golden tests, конфигурация которых лежит в папке [golden](./golden)

CI:

``` yaml
lab3:
  stage: test
  image:
    name: ryukzak/python-tools
    entrypoint: [""]
  script:
    - poetry install
    - coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t coverage report
    - ruff format --check .
```

где:

- `ryukzak/python-tools` -- docker образ, который содержит все необходимые для проверки утилиты.
  Подробнее: [Dockerfile](/src/brailfuck/Dockerfile)
- `poetry` -- управления зависимостями для языка программирования Python.
- `coverage` -- формирование отчёта об уровне покрытия исходного кода.
- `pytest` -- утилита для запуска тестов.
- `ruff` -- утилита для форматирования и проверки стиля кодирования.

Пример использования и журнал работы процессора на примере `cat`:

Пример использования для моего языка:

```shell
$ ./translator.py examples/asm/cat.asm target.txt
$ ./machine.py target.txt examples/input/cat.txt

```

Выводится листинг всех регистров.

- Значения всех регистров, кроме PS и CR выводятся в десятичном формате
- Значение регистра `PS` выводится в двоичном формате для удобного определения флагов
- В качестве значения регистра `CR`выводятся код оператора и операнд (при наличии)
- Если в какой-то регистр записан символ, в листинге выводится его код

Программа выведет:
``` shell
TICK:    1 | AC 72      | IP: 26   | AR: 4    | PS: 001 | DR: 0       | mem[AR] 0       | CR: in 4         | #DR -> AC
TICK:    2 | AC 72      | IP: 27   | AR: 3    | PS: 001 | DR: 0       | mem[AR] 0       | CR: cmp 3        | 
TICK:    3 | AC 72      | IP: 28   | AR: 34   | PS: 001 | DR: 0       | mem[AR] 0       | CR: jmz 34       | 
TICK:    5 | AC 72      | IP: 29   | AR: 35   | PS: 001 | DR: 72      | mem[AR] 72      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:    6 | AC 72      | IP: 30   | AR: 5    | PS: 001 | DR: 0       | mem[AR] 0       | CR: out 5        | AC -> #DR
TICK:    7 | AC 35      | IP: 31   | AR: 2    | PS: 000 | DR: 35      | mem[AR] 35      | CR: load 2       | DR -> AC
TICK:    8 | AC 36      | IP: 32   | AR: 31   | PS: 000 | DR: 35      | mem[AR] 0       | CR: inc          | 
TICK:   10 | AC 36      | IP: 33   | AR: 2    | PS: 000 | DR: 36      | mem[AR] 36      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:   11 | AC 36      | IP: 25   | AR: 25   | PS: 000 | DR: 0       | mem[AR] 0       | CR: jmp 25       | AR -> IP
TICK:   12 | AC 101     | IP: 26   | AR: 4    | PS: 000 | DR: 0       | mem[AR] 0       | CR: in 4         | #DR -> AC
TICK:   13 | AC 101     | IP: 27   | AR: 3    | PS: 001 | DR: 0       | mem[AR] 0       | CR: cmp 3        | 
TICK:   14 | AC 101     | IP: 28   | AR: 34   | PS: 001 | DR: 0       | mem[AR] 0       | CR: jmz 34       | 
TICK:   16 | AC 101     | IP: 29   | AR: 36   | PS: 001 | DR: 101     | mem[AR] 101     | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:   17 | AC 101     | IP: 30   | AR: 5    | PS: 001 | DR: 0       | mem[AR] 0       | CR: out 5        | AC -> #DR
TICK:   18 | AC 36      | IP: 31   | AR: 2    | PS: 000 | DR: 36      | mem[AR] 36      | CR: load 2       | DR -> AC
TICK:   19 | AC 37      | IP: 32   | AR: 31   | PS: 000 | DR: 36      | mem[AR] 0       | CR: inc          | 
TICK:   21 | AC 37      | IP: 33   | AR: 2    | PS: 000 | DR: 37      | mem[AR] 37      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:   22 | AC 37      | IP: 25   | AR: 25   | PS: 000 | DR: 0       | mem[AR] 0       | CR: jmp 25       | AR -> IP
TICK:   23 | AC 108     | IP: 26   | AR: 4    | PS: 000 | DR: 0       | mem[AR] 0       | CR: in 4         | #DR -> AC
TICK:   24 | AC 108     | IP: 27   | AR: 3    | PS: 001 | DR: 0       | mem[AR] 0       | CR: cmp 3        | 
TICK:   25 | AC 108     | IP: 28   | AR: 34   | PS: 001 | DR: 0       | mem[AR] 0       | CR: jmz 34       | 
TICK:   27 | AC 108     | IP: 29   | AR: 37   | PS: 001 | DR: 108     | mem[AR] 108     | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:   28 | AC 108     | IP: 30   | AR: 5    | PS: 001 | DR: 0       | mem[AR] 0       | CR: out 5        | AC -> #DR
TICK:   29 | AC 37      | IP: 31   | AR: 2    | PS: 000 | DR: 37      | mem[AR] 37      | CR: load 2       | DR -> AC
TICK:   30 | AC 38      | IP: 32   | AR: 31   | PS: 000 | DR: 37      | mem[AR] 0       | CR: inc          | 
TICK:   32 | AC 38      | IP: 33   | AR: 2    | PS: 000 | DR: 38      | mem[AR] 38      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:   33 | AC 38      | IP: 25   | AR: 25   | PS: 000 | DR: 0       | mem[AR] 0       | CR: jmp 25       | AR -> IP
TICK:   34 | AC 108     | IP: 26   | AR: 4    | PS: 000 | DR: 0       | mem[AR] 0       | CR: in 4         | #DR -> AC
TICK:   35 | AC 108     | IP: 27   | AR: 3    | PS: 001 | DR: 0       | mem[AR] 0       | CR: cmp 3        | 
TICK:   36 | AC 108     | IP: 28   | AR: 34   | PS: 001 | DR: 0       | mem[AR] 0       | CR: jmz 34       | 
TICK:   38 | AC 108     | IP: 29   | AR: 38   | PS: 001 | DR: 108     | mem[AR] 108     | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:   39 | AC 108     | IP: 30   | AR: 5    | PS: 001 | DR: 0       | mem[AR] 0       | CR: out 5        | AC -> #DR
TICK:   40 | AC 38      | IP: 31   | AR: 2    | PS: 000 | DR: 38      | mem[AR] 38      | CR: load 2       | DR -> AC
TICK:   41 | AC 39      | IP: 32   | AR: 31   | PS: 000 | DR: 38      | mem[AR] 0       | CR: inc          | 
TICK:   43 | AC 39      | IP: 33   | AR: 2    | PS: 000 | DR: 39      | mem[AR] 39      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:   44 | AC 39      | IP: 25   | AR: 25   | PS: 000 | DR: 0       | mem[AR] 0       | CR: jmp 25       | AR -> IP
TICK:   45 | AC 111     | IP: 26   | AR: 4    | PS: 000 | DR: 0       | mem[AR] 0       | CR: in 4         | #DR -> AC
TICK:   46 | AC 111     | IP: 27   | AR: 3    | PS: 001 | DR: 0       | mem[AR] 0       | CR: cmp 3        | 
TICK:   47 | AC 111     | IP: 28   | AR: 34   | PS: 001 | DR: 0       | mem[AR] 0       | CR: jmz 34       | 
TICK:   49 | AC 111     | IP: 29   | AR: 39   | PS: 001 | DR: 111     | mem[AR] 111     | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:   50 | AC 111     | IP: 30   | AR: 5    | PS: 001 | DR: 0       | mem[AR] 0       | CR: out 5        | AC -> #DR
TICK:   51 | AC 39      | IP: 31   | AR: 2    | PS: 000 | DR: 39      | mem[AR] 39      | CR: load 2       | DR -> AC
TICK:   52 | AC 40      | IP: 32   | AR: 31   | PS: 000 | DR: 39      | mem[AR] 0       | CR: inc          | 
TICK:   54 | AC 40      | IP: 33   | AR: 2    | PS: 000 | DR: 40      | mem[AR] 40      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:   55 | AC 40      | IP: 25   | AR: 25   | PS: 000 | DR: 0       | mem[AR] 0       | CR: jmp 25       | AR -> IP
TICK:   56 | AC 32      | IP: 26   | AR: 4    | PS: 000 | DR: 0       | mem[AR] 0       | CR: in 4         | #DR -> AC
TICK:   57 | AC 32      | IP: 27   | AR: 3    | PS: 001 | DR: 0       | mem[AR] 0       | CR: cmp 3        | 
TICK:   58 | AC 32      | IP: 28   | AR: 34   | PS: 001 | DR: 0       | mem[AR] 0       | CR: jmz 34       | 
TICK:   60 | AC 32      | IP: 29   | AR: 40   | PS: 001 | DR: 32      | mem[AR] 32      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:   61 | AC 32      | IP: 30   | AR: 5    | PS: 001 | DR: 0       | mem[AR] 0       | CR: out 5        | AC -> #DR
TICK:   62 | AC 40      | IP: 31   | AR: 2    | PS: 000 | DR: 40      | mem[AR] 40      | CR: load 2       | DR -> AC
TICK:   63 | AC 41      | IP: 32   | AR: 31   | PS: 000 | DR: 40      | mem[AR] 0       | CR: inc          | 
TICK:   65 | AC 41      | IP: 33   | AR: 2    | PS: 000 | DR: 41      | mem[AR] 41      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:   66 | AC 41      | IP: 25   | AR: 25   | PS: 000 | DR: 0       | mem[AR] 0       | CR: jmp 25       | AR -> IP
TICK:   67 | AC 102     | IP: 26   | AR: 4    | PS: 000 | DR: 0       | mem[AR] 0       | CR: in 4         | #DR -> AC
TICK:   68 | AC 102     | IP: 27   | AR: 3    | PS: 001 | DR: 0       | mem[AR] 0       | CR: cmp 3        | 
TICK:   69 | AC 102     | IP: 28   | AR: 34   | PS: 001 | DR: 0       | mem[AR] 0       | CR: jmz 34       | 
TICK:   71 | AC 102     | IP: 29   | AR: 41   | PS: 001 | DR: 102     | mem[AR] 102     | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:   72 | AC 102     | IP: 30   | AR: 5    | PS: 001 | DR: 0       | mem[AR] 0       | CR: out 5        | AC -> #DR
TICK:   73 | AC 41      | IP: 31   | AR: 2    | PS: 000 | DR: 41      | mem[AR] 41      | CR: load 2       | DR -> AC
TICK:   74 | AC 42      | IP: 32   | AR: 31   | PS: 000 | DR: 41      | mem[AR] 0       | CR: inc          | 
TICK:   76 | AC 42      | IP: 33   | AR: 2    | PS: 000 | DR: 42      | mem[AR] 42      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:   77 | AC 42      | IP: 25   | AR: 25   | PS: 000 | DR: 0       | mem[AR] 0       | CR: jmp 25       | AR -> IP
TICK:   78 | AC 114     | IP: 26   | AR: 4    | PS: 000 | DR: 0       | mem[AR] 0       | CR: in 4         | #DR -> AC
TICK:   79 | AC 114     | IP: 27   | AR: 3    | PS: 001 | DR: 0       | mem[AR] 0       | CR: cmp 3        | 
TICK:   80 | AC 114     | IP: 28   | AR: 34   | PS: 001 | DR: 0       | mem[AR] 0       | CR: jmz 34       | 
TICK:   82 | AC 114     | IP: 29   | AR: 42   | PS: 001 | DR: 114     | mem[AR] 114     | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:   83 | AC 114     | IP: 30   | AR: 5    | PS: 001 | DR: 0       | mem[AR] 0       | CR: out 5        | AC -> #DR
TICK:   84 | AC 42      | IP: 31   | AR: 2    | PS: 000 | DR: 42      | mem[AR] 42      | CR: load 2       | DR -> AC
TICK:   85 | AC 43      | IP: 32   | AR: 31   | PS: 000 | DR: 42      | mem[AR] 0       | CR: inc          | 
TICK:   87 | AC 43      | IP: 33   | AR: 2    | PS: 000 | DR: 43      | mem[AR] 43      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:   88 | AC 43      | IP: 25   | AR: 25   | PS: 000 | DR: 0       | mem[AR] 0       | CR: jmp 25       | AR -> IP
TICK:   89 | AC 111     | IP: 26   | AR: 4    | PS: 000 | DR: 0       | mem[AR] 0       | CR: in 4         | #DR -> AC
TICK:   90 | AC 111     | IP: 27   | AR: 3    | PS: 001 | DR: 0       | mem[AR] 0       | CR: cmp 3        | 
TICK:   91 | AC 111     | IP: 28   | AR: 34   | PS: 001 | DR: 0       | mem[AR] 0       | CR: jmz 34       | 
TICK:   93 | AC 111     | IP: 29   | AR: 43   | PS: 001 | DR: 111     | mem[AR] 111     | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:   94 | AC 111     | IP: 30   | AR: 5    | PS: 001 | DR: 0       | mem[AR] 0       | CR: out 5        | AC -> #DR
TICK:   95 | AC 43      | IP: 31   | AR: 2    | PS: 000 | DR: 43      | mem[AR] 43      | CR: load 2       | DR -> AC
TICK:   96 | AC 44      | IP: 32   | AR: 31   | PS: 000 | DR: 43      | mem[AR] 0       | CR: inc          | 
TICK:   98 | AC 44      | IP: 33   | AR: 2    | PS: 000 | DR: 44      | mem[AR] 44      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:   99 | AC 44      | IP: 25   | AR: 25   | PS: 000 | DR: 0       | mem[AR] 0       | CR: jmp 25       | AR -> IP
TICK:  100 | AC 109     | IP: 26   | AR: 4    | PS: 000 | DR: 0       | mem[AR] 0       | CR: in 4         | #DR -> AC
TICK:  101 | AC 109     | IP: 27   | AR: 3    | PS: 001 | DR: 0       | mem[AR] 0       | CR: cmp 3        | 
TICK:  102 | AC 109     | IP: 28   | AR: 34   | PS: 001 | DR: 0       | mem[AR] 0       | CR: jmz 34       | 
TICK:  104 | AC 109     | IP: 29   | AR: 44   | PS: 001 | DR: 109     | mem[AR] 109     | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:  105 | AC 109     | IP: 30   | AR: 5    | PS: 001 | DR: 0       | mem[AR] 0       | CR: out 5        | AC -> #DR
TICK:  106 | AC 44      | IP: 31   | AR: 2    | PS: 000 | DR: 44      | mem[AR] 44      | CR: load 2       | DR -> AC
TICK:  107 | AC 45      | IP: 32   | AR: 31   | PS: 000 | DR: 44      | mem[AR] 0       | CR: inc          | 
TICK:  109 | AC 45      | IP: 33   | AR: 2    | PS: 000 | DR: 45      | mem[AR] 45      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:  110 | AC 45      | IP: 25   | AR: 25   | PS: 000 | DR: 0       | mem[AR] 0       | CR: jmp 25       | AR -> IP
TICK:  111 | AC 32      | IP: 26   | AR: 4    | PS: 000 | DR: 0       | mem[AR] 0       | CR: in 4         | #DR -> AC
TICK:  112 | AC 32      | IP: 27   | AR: 3    | PS: 001 | DR: 0       | mem[AR] 0       | CR: cmp 3        | 
TICK:  113 | AC 32      | IP: 28   | AR: 34   | PS: 001 | DR: 0       | mem[AR] 0       | CR: jmz 34       | 
TICK:  115 | AC 32      | IP: 29   | AR: 45   | PS: 001 | DR: 32      | mem[AR] 32      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:  116 | AC 32      | IP: 30   | AR: 5    | PS: 001 | DR: 0       | mem[AR] 0       | CR: out 5        | AC -> #DR
TICK:  117 | AC 45      | IP: 31   | AR: 2    | PS: 000 | DR: 45      | mem[AR] 45      | CR: load 2       | DR -> AC
TICK:  118 | AC 46      | IP: 32   | AR: 31   | PS: 000 | DR: 45      | mem[AR] 0       | CR: inc          | 
TICK:  120 | AC 46      | IP: 33   | AR: 2    | PS: 000 | DR: 46      | mem[AR] 46      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:  121 | AC 46      | IP: 25   | AR: 25   | PS: 000 | DR: 0       | mem[AR] 0       | CR: jmp 25       | AR -> IP
TICK:  122 | AC 105     | IP: 26   | AR: 4    | PS: 000 | DR: 0       | mem[AR] 0       | CR: in 4         | #DR -> AC
TICK:  123 | AC 105     | IP: 27   | AR: 3    | PS: 001 | DR: 0       | mem[AR] 0       | CR: cmp 3        | 
TICK:  124 | AC 105     | IP: 28   | AR: 34   | PS: 001 | DR: 0       | mem[AR] 0       | CR: jmz 34       | 
TICK:  126 | AC 105     | IP: 29   | AR: 46   | PS: 001 | DR: 105     | mem[AR] 105     | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:  127 | AC 105     | IP: 30   | AR: 5    | PS: 001 | DR: 0       | mem[AR] 0       | CR: out 5        | AC -> #DR
TICK:  128 | AC 46      | IP: 31   | AR: 2    | PS: 000 | DR: 46      | mem[AR] 46      | CR: load 2       | DR -> AC
TICK:  129 | AC 47      | IP: 32   | AR: 31   | PS: 000 | DR: 46      | mem[AR] 0       | CR: inc          | 
TICK:  131 | AC 47      | IP: 33   | AR: 2    | PS: 000 | DR: 47      | mem[AR] 47      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:  132 | AC 47      | IP: 25   | AR: 25   | PS: 000 | DR: 0       | mem[AR] 0       | CR: jmp 25       | AR -> IP
TICK:  133 | AC 110     | IP: 26   | AR: 4    | PS: 000 | DR: 0       | mem[AR] 0       | CR: in 4         | #DR -> AC
TICK:  134 | AC 110     | IP: 27   | AR: 3    | PS: 001 | DR: 0       | mem[AR] 0       | CR: cmp 3        | 
TICK:  135 | AC 110     | IP: 28   | AR: 34   | PS: 001 | DR: 0       | mem[AR] 0       | CR: jmz 34       | 
TICK:  137 | AC 110     | IP: 29   | AR: 47   | PS: 001 | DR: 110     | mem[AR] 110     | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:  138 | AC 110     | IP: 30   | AR: 5    | PS: 001 | DR: 0       | mem[AR] 0       | CR: out 5        | AC -> #DR
TICK:  139 | AC 47      | IP: 31   | AR: 2    | PS: 000 | DR: 47      | mem[AR] 47      | CR: load 2       | DR -> AC
TICK:  140 | AC 48      | IP: 32   | AR: 31   | PS: 000 | DR: 47      | mem[AR] 0       | CR: inc          | 
TICK:  142 | AC 48      | IP: 33   | AR: 2    | PS: 000 | DR: 48      | mem[AR] 48      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:  143 | AC 48      | IP: 25   | AR: 25   | PS: 000 | DR: 0       | mem[AR] 0       | CR: jmp 25       | AR -> IP
TICK:  144 | AC 112     | IP: 26   | AR: 4    | PS: 000 | DR: 0       | mem[AR] 0       | CR: in 4         | #DR -> AC
TICK:  145 | AC 112     | IP: 27   | AR: 3    | PS: 001 | DR: 0       | mem[AR] 0       | CR: cmp 3        | 
TICK:  146 | AC 112     | IP: 28   | AR: 34   | PS: 001 | DR: 0       | mem[AR] 0       | CR: jmz 34       | 
TICK:  148 | AC 112     | IP: 29   | AR: 48   | PS: 001 | DR: 112     | mem[AR] 112     | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:  149 | AC 112     | IP: 30   | AR: 5    | PS: 001 | DR: 0       | mem[AR] 0       | CR: out 5        | AC -> #DR
TICK:  150 | AC 48      | IP: 31   | AR: 2    | PS: 000 | DR: 48      | mem[AR] 48      | CR: load 2       | DR -> AC
TICK:  151 | AC 49      | IP: 32   | AR: 31   | PS: 000 | DR: 48      | mem[AR] 0       | CR: inc          | 
TICK:  153 | AC 49      | IP: 33   | AR: 2    | PS: 000 | DR: 49      | mem[AR] 49      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:  154 | AC 49      | IP: 25   | AR: 25   | PS: 000 | DR: 0       | mem[AR] 0       | CR: jmp 25       | AR -> IP
TICK:  155 | AC 117     | IP: 26   | AR: 4    | PS: 000 | DR: 0       | mem[AR] 0       | CR: in 4         | #DR -> AC
TICK:  156 | AC 117     | IP: 27   | AR: 3    | PS: 001 | DR: 0       | mem[AR] 0       | CR: cmp 3        | 
TICK:  157 | AC 117     | IP: 28   | AR: 34   | PS: 001 | DR: 0       | mem[AR] 0       | CR: jmz 34       | 
TICK:  159 | AC 117     | IP: 29   | AR: 49   | PS: 001 | DR: 117     | mem[AR] 117     | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:  160 | AC 117     | IP: 30   | AR: 5    | PS: 001 | DR: 0       | mem[AR] 0       | CR: out 5        | AC -> #DR
TICK:  161 | AC 49      | IP: 31   | AR: 2    | PS: 000 | DR: 49      | mem[AR] 49      | CR: load 2       | DR -> AC
TICK:  162 | AC 50      | IP: 32   | AR: 31   | PS: 000 | DR: 49      | mem[AR] 0       | CR: inc          | 
TICK:  164 | AC 50      | IP: 33   | AR: 2    | PS: 000 | DR: 50      | mem[AR] 50      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:  165 | AC 50      | IP: 25   | AR: 25   | PS: 000 | DR: 0       | mem[AR] 0       | CR: jmp 25       | AR -> IP
TICK:  166 | AC 116     | IP: 26   | AR: 4    | PS: 000 | DR: 0       | mem[AR] 0       | CR: in 4         | #DR -> AC
TICK:  167 | AC 116     | IP: 27   | AR: 3    | PS: 001 | DR: 0       | mem[AR] 0       | CR: cmp 3        | 
TICK:  168 | AC 116     | IP: 28   | AR: 34   | PS: 001 | DR: 0       | mem[AR] 0       | CR: jmz 34       | 
TICK:  170 | AC 116     | IP: 29   | AR: 50   | PS: 001 | DR: 116     | mem[AR] 116     | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:  171 | AC 116     | IP: 30   | AR: 5    | PS: 001 | DR: 0       | mem[AR] 0       | CR: out 5        | AC -> #DR
TICK:  172 | AC 50      | IP: 31   | AR: 2    | PS: 000 | DR: 50      | mem[AR] 50      | CR: load 2       | DR -> AC
TICK:  173 | AC 51      | IP: 32   | AR: 31   | PS: 000 | DR: 50      | mem[AR] 0       | CR: inc          | 
TICK:  175 | AC 51      | IP: 33   | AR: 2    | PS: 000 | DR: 51      | mem[AR] 51      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:  176 | AC 51      | IP: 25   | AR: 25   | PS: 000 | DR: 0       | mem[AR] 0       | CR: jmp 25       | AR -> IP
TICK:  177 | AC 33      | IP: 26   | AR: 4    | PS: 000 | DR: 0       | mem[AR] 0       | CR: in 4         | #DR -> AC
TICK:  178 | AC 33      | IP: 27   | AR: 3    | PS: 001 | DR: 0       | mem[AR] 0       | CR: cmp 3        | 
TICK:  179 | AC 33      | IP: 28   | AR: 34   | PS: 001 | DR: 0       | mem[AR] 0       | CR: jmz 34       | 
TICK:  181 | AC 33      | IP: 29   | AR: 51   | PS: 001 | DR: 33      | mem[AR] 33      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:  182 | AC 33      | IP: 30   | AR: 5    | PS: 001 | DR: 0       | mem[AR] 0       | CR: out 5        | AC -> #DR
TICK:  183 | AC 51      | IP: 31   | AR: 2    | PS: 000 | DR: 51      | mem[AR] 51      | CR: load 2       | DR -> AC
TICK:  184 | AC 52      | IP: 32   | AR: 31   | PS: 000 | DR: 51      | mem[AR] 0       | CR: inc          | 
TICK:  186 | AC 52      | IP: 33   | AR: 2    | PS: 000 | DR: 52      | mem[AR] 52      | CR: store 2      | AC -> DR, DR -> mem[AR]
TICK:  187 | AC 52      | IP: 25   | AR: 25   | PS: 000 | DR: 0       | mem[AR] 0       | CR: jmp 25       | AR -> IP
TICK:  188 | AC 0       | IP: 26   | AR: 4    | PS: 000 | DR: 0       | mem[AR] 0       | CR: in 4         | #DR -> AC
TICK:  189 | AC 0       | IP: 27   | AR: 3    | PS: 011 | DR: 0       | mem[AR] 0       | CR: cmp 3        | 
TICK:  191 | AC 0       | IP: 34   | AR: 34   | PS: 011 | DR: 0       | mem[AR] 0       | CR: jmz 34       | AR -> IP
Output: Hello from input!
Instruction number: 156
Ticks: 191
```
## Итог
```text
| ФИО              | алг              | LoC | code байт | code инстр. | инстр. | такт. | вариант |
| Привалов Ярослав | hello            | 24  | -         | 24          | 99     | 112   | asm | acc | neum | hw | instr | struct | stream | port | cstr | prob5| [4]char     |
| Привалов Ярослав | cat              | 13  | -         | 13          | 165    | 202   | asm | acc | neum | hw | instr | struct | stream | port | cstr | prob5| [4]char     |
| Привалов Ярослав | hello_username   | 70  | -         | 70          | 372    | 434   | asm | acc | neum | hw | instr | struct | stream | port | cstr | prob5| [4]char     |
| Привалов Ярослав | prob5            | 40  | -         | 42          | 50595  | 65128 | asm | acc | neum | hw | instr | struct | stream | port | cstr | prob5| [4]char     |

```
