branch_commands = ["jmp", "jmn", "jmnn", "jmz", "jmnz", "jmc", "jmnc"]
branch_flags = [None, "N", "!N", "Z", "!Z", "C", "!C"]
op_commands = ["add", "sub", "load", "store", "cmp", "out", "in"] + branch_commands
nop_commands = ["hlt", "cla", "asl", "asr", "inc", "dec", "nop"]

WORD = 32
MAX_VALUE = 2 ** (WORD - 1)
U_MAX_VALUE = 2**WORD
MAX_ADDR = 2**11 - 1


def is_op_command(text):
    return text in op_commands


def is_nop_command(text):
    return text in nop_commands


def read_code(source):
    with open(source) as file:
        data = file.read()
        array_of_dicts = eval(data)
        start_address = array_of_dicts[0]["start_addr"]
        array_of_dicts.remove(array_of_dicts[0])
    return int(start_address), array_of_dicts


def write_code(code_target, start_address, code):
    with open(code_target, encoding="utf-8", mode="w") as f:
        f.write("[\n")
        f.write("{'start_addr': " + str(start_address) + " },\n")
        for i in range(len(code)):
            line = code[i]
            f.write(str(line))
            if i != len(code) - 1:
                f.write(",\n")
        f.write("]\n")
