org 2
hello: 'Hello world!\0'
addr: hello
null_terminator: '\\0'

org 25
.start: load (addr)
        cmp null_terminator
        jmz end
        out 5
        load addr
        inc
        store addr
        jmp start
    .end: hlt