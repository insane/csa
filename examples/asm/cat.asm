org 2
input_addr: input
null_terminator: '\\0'

org 25
.start: in 4
        cmp null_terminator
        jmz end
        store (input_addr)
        out 5
        load input_addr
        inc
        store input_addr
        jmp start
    .end: hlt

input: 0