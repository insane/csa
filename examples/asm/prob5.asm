org 2
dividend: 0
divisor: 0
remainder: 0
rem_flag: 0
first_num: 10
second_num: 9
tmp: 9
result: 0

div_ret: dret

org 100
.div: load dividend
    sub divisor
    store dividend
    store remainder
    jmn (div_ret)
    jmz (div_ret)
    jmp div


org 200
.start: load result
    add tmp
    store dividend
    store result
    point: load result
    store dividend
    load second_num
    store divisor
    jmp div



.dret: load remainder
    jmn start
    load first_num
    dec
    store first_num
    load second_num
    dec
    jmz end
    store second_num
    load result
    store tmp
    load remainder
    jmz point
    jmp start

    .end: load result
    out 5
    hlt
