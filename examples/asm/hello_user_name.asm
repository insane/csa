org 2
ask: 'What is your name?\0'
greet: 'Hello, '

null_terminator: '\\0'

ask_addr: ask
ask_ret: read_name

greet_addr: greet
greet_ret: print_name

name_addr: name
name_ret: end
name_counter: name

print_addr: 0
print_ret: 0

org 125
.print: load (print_addr)
        cmp null_terminator
        jmz (print_ret)
        out 5
        load print_addr
        inc
        store print_addr
        jmp print

.read: in 4
        cmp null_terminator
        jmz print_greet
        store (name_counter)
        load name_counter
        inc
        store name_counter
        jmp read

org 200
.start: load ask_ret
    store print_ret
    load ask_addr
    store print_addr
    jmp print

    .read_name: jmp read

    .print_greet: load greet_ret
    store print_ret
    load greet_addr
    store print_addr
    jmp print

    .print_name: load name_ret
    store print_ret
    load name_addr
    store print_addr
    jmp print

    .end: hlt

name: 0